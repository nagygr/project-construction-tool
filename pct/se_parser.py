# The MIT License (MIT)
# 
# Copyright (c) 2013, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import sys
import copy
import string

class node(object):
	def __init__(self, a_value = "", parent = None):
		self.value = a_value
		self.parent = parent
		self.children = []

	def add_child(self, value = ""):
		self.children.append(node(value, self))

class tree_printer(object):
	def __init__(self):
		self.__counter = 0

	def print_tree(self, node):
		for i in range(self.__counter):
			sys.stdout.write("\t")

		print node.value

		self.__counter += 1

		for child in node.children:
			self.print_tree(child)

		self.__counter -= 1

class se_parser(object):
	def __init__(self, filename):
		self.__filename = filename
		self.ast = node()
		self.__root = self.ast
		self.__contents = ""

		if (not os.path.isfile(filename)):
			raise "Configuration file (" + filename + ") not found"

		f = open(filename)
		self.__contents = f.read()
		f.close()

	def parse(self):
		index = [0]
		return self.s_expression(index)

	def s_expression(self, index):
		result = [""]

		self.whitespace(index)

		if (self.character(index, "(", result)):
			self.__root.add_child()
			self.__root = self.__root.children[-1]

			is_inside = True
			while (is_inside):
				result = [""]

				if (self.identifier(index, "_", result)):
					self.__root.add_child(result[0])
				elif (self.string(index, "\"", "\\", result)):
					self.__root.add_child(result[0])
				elif (self.s_expression(index)):
					pass
				else:
					is_inside = False

			if (self.character(index, ")", result)):
				self.__root = self.__root.parent
				return True

		return False

	def whitespace(self, index):
		while (self.__contents[index[0]].isspace()):
			index[0] += 1

		return True

	def character(self, index, characters, result):
		self.whitespace(index)
		
		if (self.__contents[index[0]] in characters):
			result[0] = self.__contents[index[0]]
			index[0] += 1
			return True

		return False

	def identifier(self, index, extra_characters, result):
		locidx = copy.deepcopy(index)
		self.whitespace(locidx)

		if (self.__contents[locidx[0]] in string.letters or self.__contents[locidx[0]] in extra_characters):
			locidx[0] += 1
			while (self.__contents[locidx[0]] in string.letters or self.__contents[locidx[0]] in string.digits or self.__contents[locidx[0]] in extra_characters):
				locidx[0] += 1

			result[0] = self.__contents[index[0] : locidx[0]]
			index[0] = locidx[0]
			return True

		return False

	def string(self, index, delimiter, escape, result):
		locidx = copy.deepcopy(index)
		self.whitespace(locidx)

		if (self.__contents[locidx[0]] == delimiter):
			start_index = copy.deepcopy(locidx)
			locidx[0] += 1

			end_reached = False

			while (not end_reached):
				if (self.__contents[locidx[0]] == escape):
					locidx[0] += 2

				elif (self.__contents[locidx[0]] == delimiter):
					end_reached = True

				else:
					locidx[0] += 1

			result[0] = self.__contents[start_index[0] + 1 : locidx[0]]
			index[0] = locidx[0] + 1

			return True
		
		return False
