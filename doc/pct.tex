% The MIT License (MIT)
% 
% Copyright (c) 2013, Gergely Nagy
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
\documentclass[12pt]{article}
\usepackage[paper=a4paper, margin=2.5cm]{geometry}
\usepackage{fancyhdr}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[pdfborder={0 0 0}]{hyperref}
\usepackage{setspace}
\usepackage[T1]{fontenc}
\usepackage{hfoldsty} 
\usepackage{listings}
\usepackage{bold-extra}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhf[FL]{\scriptsize{\textit{Project Construction Tool}}}
\fancyhf[FC]{\scriptsize{\textit{\textbf{\thepage}}}}
\fancyhf[FR]{\scriptsize{\textit{\today}}}

\lstset{basicstyle=\scriptsize\ttfamily, stringstyle=\ttfamily,
keywordstyle=\bfseries\ttfamily,aboveskip=10pt,
belowskip=10pt, tabsize=2, xleftmargin=5pt, captionpos=b, 
literate={á}{{\'a}}1 {é}{{\'e}}1 {ó}{{\'o}}1 {ö}{{\"o}}1 {ú}{{\'u}}1 {ü}{{\"u}}1}

\begin{document}
\sloppy

\begin{flushright}
	\begin{minipage}[h]{0.3\linewidth}
		\begin{spacing}{2}
			{\Huge
				\textbf{Project\\Construction\\Tool}
			}
		\end{spacing}
	\end{minipage}
\end{flushright}

\vspace{1em}

\begin{center}
	\begin{minipage}[h]{0.85\linewidth}
		\itshape
		This is a simple, scriptable, automated project construction tool for C/C++ projects. It finds the
		dependencies in the project and calls the compiler accordingly. The compiler and the flags to be used
		can be set in a configuration file.
	\end{minipage}
\end{center}

\vspace{4.5em}

\section*{Dependencies}
Two types of relationship between the files constituting the project are explored. One that concerns the
up-to-dateness of a source file, the other the connections between the object files -- the \emph{linking
dependencies}.

Whenever a source file or any of the header files that are included to it are altered, the source file needs
to be recompiled to an object file. So the included headers need to be gathered. This doesn't only mean the
collection of includes in the file itself, as the file also depends on headers included by headers, so the
assemblage has to be done recursively.

An object has to be linked to every other object file in the project that defines types or functions that are
used in the given file. This kind of relationship is analyzed based on the includes as well, but in this case
the source files corresponding to the headers have to be explored.

The tool assumes that the project is organized as follows: a header file can have a source file where the
declared types and functions are defined. If it does, then the name of the source file is equivalent to that
of the header aside from the extension. Preferably every type has its own header file and the name of the
file matches exactly that of the type but this is not necessary for the tool to work.

The walk that is performed in this step starts with the headers of a source file. It goes on to the source
files of the headers, includes them in the dependency list and then proceeds to the source files of the
headers included into the current source file and so on.

The entire process is based on includes so global variables referred to as \emph{extern}s are not investigated
and thus these relationships are not discovered.

\section*{The structure of a project}
The tool expects the source files to reside in a dedicated directory. No further directory hierarchy is
required. The header files and the source files can be placed in different directories but this is not
necessary. The object files and the binaries are created in dedicated directories as well so the products of
the project are not mixed with the sources. This can be advantageous if the project is version controlled and
objects and binaries are not tracked but it also makes the structure clear and easier to maintain.

The tool will also automatically create every necessary directory (aside from the source directories which
have to exist).

A possible (though not demanded) structure for C++ projects that is also handled by the tool is the following.
Every class resides in a header and source file that hold its name (\emph{e.g.} \texttt{class Example} is
found in \texttt{Example.hpp} and \texttt{Example.cpp}).

The hierarchy of namespaces corresponds to the structure of the project. For example an XML parser class that is
used to process the configuration file of the project could be located in the \texttt{utility::parsers}
namespace. So the fully qualified name of the class is \texttt{utility::parsers::XMLParser}. The directory
hierarchy of the project resembles that of the namespace structure: there is a directory called
\texttt{utility} under the source directory which contains a \texttt{parser} directory that holds the files
\texttt{XMLParser.hpp} and \texttt{XMLParser.cpp}.

The tool assumes that one sets the header directory as a standard place to look for headers (using \emph{e.g.} the
\texttt{-I} flag of \texttt{gcc}). This way include expressions in the project will reflect the virtual
structure of the classes and thus every type will be easy to locate both in the file system and in the namespace
structure:

\begin{lstlisting}[language=C++]
#include <utility/parsers/XMLParser.hpp>

int main() {
	utility::parsers::XMLParser xmlParser("configuration.xml");
	// ...

	return 0;
}
\end{lstlisting}

The tool will automatically create the necessary directories under the object and binary directories that
correspond to the directory structure of the source directory. Thus the \texttt{XMLparser} class' object
will reside in: \texttt{obj/utility/parsers/XMLParser.o} and in case it was an executable, the binary would be
found in \texttt{bin/utility/parsers/XMLParser} (provided the object directory is \texttt{obj}, the object
extension is \texttt{.o} and the binary directory is \texttt{bin} -- these can all be set in the configuration
file).

\section*{The configuration file}

The configuration file of the project is called \texttt{pctrc} and should be placed next to the tool
(\texttt{pct.py}). The entire file is an S-expression (you can read about it
\href{https://en.wikipedia.org/wiki/S-expression}{\textit{here}}), probably the simplest grammar that describes a
hierarchical structure with an arbitrary depth.

An example \texttt{pctrc} can be found below.

\begin{lstlisting}
("Example configuration"
	(compiler_name "clang++")
	(dont_link_flag "-c")
	(output_flag "-o")
	(library_flag "-l")
	(includes_path_flag "-I")
	(optimization_flag "-O")
	(optimization_level "2")
	(extra_options 
		("-Wall" "-Werror" "-pedantic")
	)
	(target_directory "bin")
	(object_directory "obj")
	(source_directory "src")
	(header_directory "src")
	(header_extension ".h")
	(source_extension ".cc")
	(object_extension ".o")
	(extra_libraries
		("math.h" "m")
		("boost/filesystem.hpp" "boost_filesystem")
		("boost/filesystem.hpp" "boost_system")
		("boost/regex.hpp" "boost_regex")
	)
)
\end{lstlisting}

The example contains every possible setting the \texttt{pctrc} may include. The names speak for themselves
save probably the \texttt{extra\_libraries} which is explained next.

In the dependencies section only internal dependencies were covered but the project may also depend on
external libraries. To find these dependencies the included headers are investigated again. If a header that
is part of an external library is included then the corresponding library has to be linked to a given object.
These header-library pairs are given in the \texttt{extra\_libraries} unit.

At the moment, the standard places to look for libraries (\texttt{-L} flag of \texttt{gcc}) can not be extended
using dedicated means, but the \texttt{extra\_options} setting can always include a string such as
\texttt{"-Llib"} which would tell the compiler to look for libraries in the project's \texttt{lib} directory.

The settings have default values (overwritten by the configuration file) that can be found in
\texttt{pct/configuration.py}.

\section*{Using the tool}

Two files should be placed in the project library: the tool (\texttt{pct.py}) and the configuration file
(\texttt{pctrc}), although the latter is optional provided the default settings are satisfactory. The
\texttt{pct.py} file is in fact a ZIP file that contains the byte codes of the python project. It can be run
using the python interpreter. One of the two possible runtime parameters have to be given as well: \emph{all}
will build the project, while \emph{clean} deletes every file and directory created by \emph{all}.

\begin{lstlisting}
$ python pct.py all
$ python pct.py clean
\end{lstlisting}

\section*{Legal issues}

The project uses the MIT license with Gergely Nagy as the copyright holder, thus it should be handled
according to its terms, that can be found at the top of every source file and also
\href{http://opensource.org/licenses/MIT}{\textit{here}}.

\vfill
\begin{center}
\emph{Please consider the environment before printing this document.}
\end{center}
\vfill
\end{document}
