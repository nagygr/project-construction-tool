.phony: all clean

all:
	python -m compileall . 
	chmod u+x `ls *.pyc` 
	zip -r pct.py `find ./ -name "*.pyc"` 

clean:
	rm `find ./ -name "*.pyc"`
	rm -f pct.py
