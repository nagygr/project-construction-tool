# The MIT License (MIT)
# 
# Copyright (c) 2013, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import re
import string
import subprocess

from code_file import code_file

class constructor(object):
	def __init__(self, the_configuration):
		self.__configuration = the_configuration
		self.__headers = []
		self.__sources = []
		self.__targets = []
		self.__source_directories = []
		self.__header_directories = []
		self.__files = {}

		for root, dirnames, filenames in os.walk(self.__configuration.source_directory):
			self.__source_directories.append(root);

			for filename in filenames:
				is_target = False;

				if (re.search(self.__configuration.source_extension + "$", filename)):
					source_path = os.path.join(root, filename);
					self.__sources.append(source_path)

					a_file = open(source_path, "r");
					for line in a_file:
						if (re.search("^\s*int\s*main\s*\(", line)):
							is_target = True;
							self.__targets.append(source_path)

		for root, dirnames, filenames in os.walk(self.__configuration.header_directory):
			self.__header_directories.append(root);

			for filename in filenames:
				if (re.search(self.__configuration.header_extension + "$", filename)):
					header_path = os.path.join(root, filename);
					self.__headers.append(header_path)

		self.find_header_dependencies()
		self.find_link_dependencies()

	def find_header_dependencies(self):
		for source in self.__sources:
			if (not(source in self.__files)):
				self.__files[source] = code_file(source)

			self.find_header_dependencies_walk(source, source)


	def find_header_dependencies_walk(self, source, current):
		sfile = open(current)
		for line in sfile:
			if (re.search("^#\s*include", line)):
				include = line[line.find("<") + 1 : line.find(">")];

				new_extra_libs = self.__configuration.get_extra_libraries(include)
				for new_extra_lib in new_extra_libs:
					self.__files[source].extra_libs.add(new_extra_lib)

				include_path = self.include_to_full_header(include)

				if (self.is_local_header(include_path)):
					self.__files[source].include_deps.add(include_path)
					self.find_header_dependencies_walk(source, include_path)

	def find_link_dependencies(self):
		for source in self.__sources:
			self.find_link_dependencies_walk(source, source)

	def find_link_dependencies_walk(self, original_source, current_source):
		self.__files[original_source].extra_libs |= self.__files[current_source].extra_libs
		for header in self.__files[current_source].include_deps:
			source_dep = self.full_header_to_full_source(header)
			if (original_source != source_dep and self.is_local_source(source_dep)):
				new_object = self.source_to_object(source_dep)
				if (new_object not in self.__files[original_source].link_deps):
					self.__files[original_source].link_deps.add(new_object)
					self.find_link_dependencies_walk(original_source, source_dep)
		
	def is_local_header(self, header_file):
		return header_file in self.__headers

	def is_local_source(self, source_file):
		return source_file in self.__sources

	def include_to_full_header(self, include):
		return os.path.join(self.__configuration.header_directory, include)

	def include_to_full_source(self, include):
		source_name = include.replace(self.__configuration.header_extension, self.__configuration.source_extension)
		return os.path.join(self.__configuration.source_directory, source_name)

	def full_header_to_full_source(self, full_header):
		hdr_dir = self.__configuration.header_directory
		src_dir = self.__configuration.source_directory
		hdr_ext = self.__configuration.header_extension
		src_ext = self.__configuration.source_extension
		return re.sub("^" + hdr_dir, src_dir, full_header).replace(hdr_ext, src_ext)

	def source_to_object(self, source):
		src_dir = self.__configuration.source_directory
		src_ext = self.__configuration.source_extension
		obj_dir = self.__configuration.object_directory
		obj_ext = self.__configuration.object_extension
		return re.sub("^" + src_dir, obj_dir, source).replace(src_ext, obj_ext)	

	def source_to_binary(self, source):
		src_dir = self.__configuration.source_directory
		src_ext = self.__configuration.source_extension
		bin_dir = self.__configuration.target_directory
		return re.sub("^" + src_dir, bin_dir, source).replace(src_ext, "")	

	def object_needs_rebuild(self, source):
		obj = self.source_to_object(source)

		if (not os.path.isfile(obj)): return True

		object_time = os.path.getmtime(obj);
		source_time = os.path.getmtime(source);

		latest_dependency_time = object_time;
		for hdr in self.__files[source].include_deps:
			hdr_time = os.path.getmtime(hdr)
			if (latest_dependency_time < hdr_time):
				latest_dependency_time = hdr_time

		return (object_time < source_time) or (object_time < latest_dependency_time)

	def binary_needs_rebuild(self, source):
		obj = self.source_to_object(source)
		binary = self.source_to_binary(source)

		if ((not os.path.isfile(obj)) or (not os.path.isfile(binary))): return True

		object_time = os.path.getmtime(obj);
		binary_time = os.path.getmtime(binary);

		latest_dependency_time = binary_time;
		for an_obj in self.__files[source].link_deps:
			an_obj_time = os.path.getmtime(an_obj)
			if (latest_dependency_time < an_obj_time):
				latest_dependency_time = an_obj_time

		return (binary_time < object_time) or (binary_time < latest_dependency_time)

	def compile_project(self):
		self.__create_directories();
		try:
			self.__create_objects();
			self.__create_targets();
		except subprocess.CalledProcessError, cpe:
			print "Compiler error [" + str(cpe.returncode) + "]: " + string.join(cpe.cmd);

	def __create_directories(self):
		src_dir = self.__configuration.source_directory
		obj_dir = self.__configuration.object_directory
		bin_dir = self.__configuration.target_directory
		
		for directory in self.__source_directories:
			for directory_name in [re.sub("^" + src_dir, obj_dir, directory), re.sub("^" + src_dir, bin_dir, directory)]:
				try:
					if (not os.path.isdir(directory_name)):
						print "Creating directory: " + directory_name;
						os.makedirs(directory_name);
				except OSError, e:
					if (e.errno != errno.EEXIST):
						raise;

	def __create_objects(self):
		call_list = []
		call_list.append(self.__configuration.compiler_name)
		call_list.extend(self.__configuration.extra_options)
		call_list.append(self.__configuration.dont_link_flag)
		call_list.append(self.__configuration.optimization_flag + self.__configuration.optimization_level)
		call_list.append(self.__configuration.includes_path_flag + self.__configuration.header_directory)

		output_flag = self.__configuration.output_flag

		for source in self.__files.values():
			if (self.object_needs_rebuild(source.name)):
				the_object = self.source_to_object(source.name)
				arguments = call_list + [output_flag, the_object, source.name] 
				print "Calling the compiler: " + string.join(arguments)
				subprocess.check_call(arguments)
			else:
				print "Object (" + self.source_to_object(source.name) + ") is at its latest"

	def __create_targets(self):
		call_list = []
		call_list.append(self.__configuration.compiler_name)
		call_list.extend(self.__configuration.extra_options)
		call_list.append(self.__configuration.optimization_flag + self.__configuration.optimization_level)

		output_flag = self.__configuration.output_flag
		library_flag = self.__configuration.library_flag

		for source in self.__files.values():
			if (source.name in self.__targets):

				if (self.binary_needs_rebuild(source.name)):
					extra_lib_links = []
					for el in source.extra_libs:
						extra_lib_links.append(library_flag + el)

					the_object = self.source_to_object(source.name)
					the_binary = self.source_to_binary(source.name)

					arguments = call_list + [output_flag, the_binary, the_object] + list(source.link_deps) + extra_lib_links
					print "Calling linker: " + string.join(arguments) 
					subprocess.check_call(arguments)
				else:
					print "Binary (" + self.source_to_binary(source.name) + ") is at its latest"


	def print_all(self):
		print "headers: ", self.__headers, "\n"
		print "sources: ", self.__sources, "\n"
		print "targets: ", self.__targets, "\n"
		print "source_directories: ", self.__source_directories, "\n"
		print "header_directories: ", self.__header_directories, "\n"
		
		print "files: "
		for a_file in self.__files.values():
			print a_file.name
			print "\tIncludes: ", a_file.include_deps
			print "\tLink deps: ", a_file.link_deps
			print "\tExtra libs: ", a_file.extra_libs
			print "\n"
