# The MIT License (MIT)
# 
# Copyright (c) 2013, Gergely Nagy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from se_parser import node 
from se_parser import tree_printer
from se_parser import se_parser

class configuration(object):
	def __init__(self, filename = ""):
		self.compiler_name = "g++"
		self.dont_link_flag = "-c"
		self.output_flag = "-o"
		self.library_flag = "-l"
		self.includes_path_flag = "-I"
		self.optimization_flag = "-O"
		self.optimization_level = "2"
		self.extra_options = ["-Wall", "-Werror", "-pedantic"]
		self.target_directory = "bin"
		self.object_directory = "obj"
		self.source_directory = "src"
		self.header_directory = "src"
		self.header_extension = ".h"
		self.source_extension = ".cc"
		self.object_extension = ".o"
		self.extra_libraries = [("math.h", "m"), ("boost/filesystem.hpp", "boost_filesystem"), ("boost/filesystem.hpp", "boost_system"), ("boost/regex.hpp", "boost_regex")]

		self.__simple_attributes = ["compiler_name", "dont_link_flag", "output_flag", "library_flag", "includes_path_flag", "optimization_flag", "optimization_level", "target_directory", "object_directory", "source_directory", "header_directory", "header_extension", "source_extension", "object_extension"]
		self.__simple_attribute_next = False
		self.__next_simple_attributes_name = ""
		self.__extra_options_next = False
		
		if (filename != ""):
			print "Processing config file (" + filename + ")\n"
			parser = se_parser(filename)
			parser.parse()
			self.parse_ast(parser.ast)

		print self

	def parse_ast(self, root):
		if (self.__simple_attribute_next):
			self.__dict__[self.__next_simple_attributes_name] = root.value
			self.__simple_attribute_next = False

		elif (self.__extra_options_next):
			self.__extra_options_next = False
			self.extra_options = []
			for child in root.children:
				self.extra_options.append(child.value)
			return

		elif (root.value in self.__simple_attributes):
			self.__simple_attribute_next = True
			self.__next_simple_attributes_name = root.value
			return

		elif (root.value == "extra_options"):
			self.__extra_options_next = True
			return

		elif (len(root.children)> 0 and root.children[0].value == "extra_libraries"):
			self.extra_libraries = []
			for child in root.children[1:]:
				self.extra_libraries.append((child.children[0].value, child.children[1].value))
			return

		for child in root.children:
			self.parse_ast(child)

	def get_extra_libraries(self, include):
		extra_libs = []

		for lib_tuple in self.extra_libraries:
			if (include == lib_tuple[0]):
				extra_libs.append(lib_tuple[1])

		return extra_libs

	def __str__(self):
		result = "=============\nConfiguration\n=============\n"
		for simple_attribute in self.__simple_attributes:
			result += "\t" + simple_attribute + ": " + self.__dict__[simple_attribute] + "\n"

		result += "\t" + "Extra options: " + " ".join(self.extra_options) + "\n"
		result += "\t" + "Extra libraries:" + "\n"

		for header, lib in self.extra_libraries:
			result += "\t\t" + header + " -> " + lib + "\n"

		return result
